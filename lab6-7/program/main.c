﻿#include <stdio.h>
#include <math.h>

int main(int argc, char * argv[]) {
	const double e = 2.718;

	const double n = 6.0;
	const double a = 0.3;
	const double b = 0.7;
	
	const double x0 = a;
	const double x1 = (a + b)/2;
	const double x2 = b;

	const double y0 = (10.3 * sin(x0)) - pow(e, (6*x0 + 5)/11);
	const double y1 = (10.3 * sin(x1)) - pow(e, (6*x1 + 5)/11);
	const double y2 = (10.3 * sin(x2)) - pow(e, (6*x2 + 5)/11);

	const double h = (b - a)/n;

	const double a0 = y0/((x0 - x1)*(x0 - x2));
	const double a1 = y1/((x1 - x0)*(x1 - x2));
	const double a2 = y2/((x2 - x0)*(x2 - x1));

	const double ht = 2.0/n;
	const double hT = ht;

	const double dy0 = y1 - y0;
	const double dy1 = y2 - y1;
	const double d2y0 = dy1 - dy0;
	
	const double sx = x0 + x1 + x2;
	const double sx2 = pow(x0, 2) + pow(x1, 2) + pow(x2, 2);
	const double sx3 = pow(x0, 3) + pow(x1, 3) + pow(x2, 3);
	const double sx4 = pow(x0, 4) + pow(x1, 4) + pow(x2, 4);

	const double sy = y0 + y1 + y2;
	const double sxy = x0*y0 + x1*y1 + x2*y2;
	const double sx2y = pow(x0, 2)*y0 + pow(x1, 2)*y1 + pow(x2, 2)*y2;

	const double d  = (3*sx2*sx4  + sx*sx3*sx2  + sx*sx3*sx2 ) - (sx2*sx2*sx2  + sx3*sx3*3  + sx*sx*sx4 );
	const double d1 = (sy*sx2*sx4 + sxy*sx3*sx2 + sx*sx3*sx2y) - (sx2*sx2*sx2y + sx3*sx3*sy + sxy*sx*sx4);
	const double d2 = (3*sxy*sx4  + sy*sx3*sx2  + sx*sx2y*sx2) - (sx2*sxy*sx2  + sx2y*sx3*3 + sx*sy*sx4 );
	const double d3 = (3*sx2*sx2y + sx*sxy*sx2  + sx*sx3*sy  ) - (sy*sx2*sx2   + sxy*sx3*3  + sx*sx*sx2y);

	const double A0 = d1/d;
	const double A1 = d2/d;
	const double A2 = d3/d;
	
	double x = x0;
	double t = 0.0;
	double T = -2.0;
	double f;
	double L;
	double N1;
	double N2;
	double P;

	printf("n\tx\tt\tT\tf(x)\tL2(x)\t|f(x)-L2(x)|\tN1(x)\t|f(x)-N1(x)|\tN2(x)\t|f(x)-N2(x)|\tP2(x)\t|f(x)-P2(x)|\n");
	for(int i = 0; i <= n; i++) {
		f = (10.3 * sin(x)) - pow(e, (6*x + 5)/11);
		L = a0*(x - x1)*(x - x2) + a1*(x - x0)*(x - x2) + a2*(x - x0)*(x - x1);
		N1 = y0 + t*dy0 + (t/2)*(t - 1)*d2y0;
		N2 = y2 + T*dy1 + (T/2)*(T + 1)*d2y0;
		P = A2*pow(x, 2) + A1*x + A0;

		printf(
			"%-2d\t%4.2f\t%4.2f\t%5.2f\t%5.2f\t%5.2f\t%6.3f\t\t%5.2f\t%6.3f\t\t%5.2f\t%6.3f\t\t%5.2f\t%6.3f\n",
			i, x, t, T, f, L, fabs(f - L), N1, fabs(f - N1), N2, fabs(f - N2), P, fabs(f - P)
		);

		x = x + h;
		t = t + ht;
		T = T + hT;
	}
	return 0;
}

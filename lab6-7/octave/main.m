pkg load symbolic
syms x y z
fid = fopen ('out.tex', "w");

## OUR
f = (10.3 * sin(x)) - e^((6*x+5)/11)

fprintf(fid, "\\newcommand{\\func}{y = 10.3 \sin(x) - e^{\\frac{6x + 5}{11}}}\n")

n = 20

x0 = 0
x1 = 0.5
x2 = 1

h = x1

y0 = double(subs(f, x, x0))
y1 = double(subs(f, x, x1))
y2 = double(subs(f, x, x2))

fprintf(fid, "\\newcommand{\\xz}{%d}\n",   x0)
fprintf(fid, "\\newcommand{\\xo}{%.1f}\n", x1)
fprintf(fid, "\\newcommand{\\xt}{%d}\n",   x2)
fprintf(fid, "\\newcommand{\\yz}{%.3f}\n", y0)
fprintf(fid, "\\newcommand{\\yo}{%.3f}\n", y1)
fprintf(fid, "\\newcommand{\\yt}{%.3f}\n", y2)

## lagrange

a0 = y0 / ((x0 - x1) * (x0 - x2))
a1 = y1 / ((x1 - x0) * (x1 - x2))
a2 = y2 / ((x2 - x0) * (x2 - x1))

fprintf(fid, "\\newcommand{\\az}{%.3f}\n", a0)
fprintf(fid, "\\newcommand{\\ao}{%.3f}\n", a1)
fprintf(fid, "\\newcommand{\\at}{%.3f}\n", a2)

## Newton I

t = 0:1:2
dy0  = y1 - y0
dy1  = y2 - y1
d2y0 = dy1 - dy0

#N1 = y0 + t * dy0 + ((t*(t - 1))/2)*d2y0

## Newton II

dt = (x - x2)/h

N2 = y0 + dt * dy0 + ((dt*(dt - 1))/(factorial(2)))* d2y0

## appromaxicionniy

P2 = a2*x^2 + a1*x + a0

xi = x0 + x1 + x2
xi2 = x0^2 + x1^2 + x2^2
xi3 = x0^3 + x1^3 + x2^3
xi4 = x0^4 + x1^4 + x2^4

fprintf(fid, "\\newcommand{\\sxon}{%.3f}\n", xi)
fprintf(fid, "\\newcommand{\\sxtw}{%.3f}\n", xi2)
fprintf(fid, "\\newcommand{\\sxth}{%.3f}\n", xi3)
fprintf(fid, "\\newcommand{\\sxfo}{%.3f}\n", xi4)

yi    = y0       + y1       + y2
xiyi  = x0*y0    + x1*y1    + x2*y2
xi2yi = x0^2 *y0 + x1^2 *y1 + x2^2 *y2

fprintf(fid, "\\newcommand{\\sy}{%.3f}\n", yi)
fprintf(fid, "\\newcommand{\\sxy}{%.3f}\n", xiyi)
fprintf(fid, "\\newcommand{\\sxxy}{%.3f}\n", xi2yi)

m = 2

#f11 = (m+1)*a0 + xi *a1 + xi2*a2 == yi
#f12 = xi   *a0 + xi2*a1 + xi3*a2 == xiyi
#f13 = xi2  *a0 + xi3*a1 + xi4*a2 == xi2yi

opr =	[(m+1), xi , xi2, yi;
		xi    , xi2, xi3, xiyi;
		xi2   , xi3, xi4, xi2yi]

d1 =	[yi   , xi , xi2;
		 xiyi , xi2, xi3;
		 xi2yi, xi3, xi4]

d2 =	[(m+1), yi   , xi2;
		 xi   , xiyi , xi3;
		 xi2  , xi2yi, xi4]

d3 =	[(m+1), xi , yi;
		 xi   , xi2, xiyi;
		 xi2  , xi3, xi2yi]

dd =	[(m+1), xi , xi2;
		 xi   , xi2, xi3;
		 xi2  , xi3, xi4]

detd1 = det(d1)
detd2 = det(d2)
detd3 = det(d3)
detdd = det(dd)

fprintf(fid, "\\newcommand{\\detdon}{%.3f}\n", detd1)
fprintf(fid, "\\newcommand{\\detdtw}{%.3f}\n", detd2)
fprintf(fid, "\\newcommand{\\detdth}{%.3f}\n", detd3)
fprintf(fid, "\\newcommand{\\detddd}{%.3f}\n", detdd)


aa0 = detd1/detdd
aa1 = detd2/detdd
aa2 = detd3/detdd

fprintf(fid, "\\newcommand{\\aaon}{%.3f}\n", aa0)
fprintf(fid, "\\newcommand{\\aatw}{%.3f}\n", aa1)
fprintf(fid, "\\newcommand{\\aath}{%.3f}\n", aa2)

fclose (fid);

#include <stdio.h>
#include <math.h>

void sim() {
	printf("\nМетод просты итераций\n");
	printf("n\tXn\tXn+1\tXn+1-Xn\tf(x)\n");
	int n = 0;
	double c = -0.2, eps = 0.001, sigma = 0.01;
	double a, b, z, x, x0, y;
	x = x0 = 1;
	do {
		y = x + c * (2 * x - 4 * cos(x) - 0.6);
		z = x;
		printf("%d\t%.4f\t%.4f\t%.4f\t%.4f\n", n++, x, y, fabs(y - x),
				fabs(2 * y - 4 * cos(y) - 0.6));
		x = y;
		a = fabs(z - x);
		b = fabs(2 * y - 4 * cos(y) - 0.6);
	} while (a > eps || b > sigma);
	printf("res:\t%.4f\n", x);
}

void nm () {
	printf("\nМетод Ньютона\n");
	printf("n\tXn\tXn+1\tXn+1-Xn\tf(x)\n");
	int n = 0;
	double eps = 0.001, sigma = 0.01;
	double a, b, z, x, x0, y;
	x = x0 = 1;
	do {
		y = x - (2 * x - 4 * cos(x) - 0.6) / (4 * sin(x) + 2);
		z = x;
		printf("%d\t%.4f\t%.4f\t%.4f\t%.4f\n", n++, x, y, fabs(y - x),
				fabs(2 * y - 4 * cos(y) - 0.6));
		x = y;
		a = fabs(z - x);
		b = fabs(2 * y - 4 * cos(y) - 0.6);
	} while (a > eps || b > sigma);
	printf("res:\t%.4f\n", x);
}

void mnm()
{
	printf("\nМодифицированный метод Ньютона\n");
	printf("n\tXn\tXn+1\tXn+1-Xn\tf(x)\n");
	int n = 0;
	double eps = 0.001, sigma = 0.01;
	double a, b, z, x, x0, y;
	x = x0 = 1;
	do {
		y = x - ((2 * x - 4 * cos(x) - 0.6) / (4 * sin(x0) + 2));
		z = x;
		printf("%d\t%.4f\t%.4f\t%.4f\t%.4f\n", n++, x, y, fabs(y - x),
				fabs(2 * y - 4 * cos(y) - 0.6));
		x = y;
		a = fabs(z - x);
		b = fabs(2 * y - 4 * cos(y) - 0.6);
	} while (a > eps || b > sigma);
	printf("res:\t%.4f\n", x);
}

int main() {
	sim();
	nm();
	mnm();
	return 0;
}

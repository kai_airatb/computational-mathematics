pkg load symbolic
syms x y z
fid = fopen ('out.tex', "w");

## PRIM
#f1 = 8*x - 5*y + z
#res1 = 1
#f2 = x + 6*y - 2*z
#res2 = 7
#f3 = -x -y + 4*z
#res3 = 9
#
#fmatrix = [ 8, -5,  1;
#            1,  6, -2;
#           -1, -1,  4]
#amatrix = [ 1;
#            7;
#            9]

## ME
f1 = 6*x - 1/8*y - 0.1*z
res1 = -0.1
f2 = -3*x - 5*y + 0.5*z
res2 = 0.5
f3 = 2*x - 3*y + 6*z
res3 = 6

fmatrix = [ 6, -1/8, -0.1;
           -3,   -5,  0.5;
            2,    3,    6]
amatrix = [ -0.1;
             0.5;
               6]


fprintf(fid, "\\newcommand{\\formulaOne}{%s}\n", latex(f1 == res1))
fprintf(fid, "\\newcommand{\\formulaTwo}{%s}\n", latex(f2 == res2))
fprintf(fid, "\\newcommand{\\formulaThr}{%s}\n", latex(f3 == res3))

slau = solve(f1 == res1, f2 == res2, f3 == res3)

fprintf(fid, "\\newcommand{\\ansX}{%s}\n", char(slau.x))
fprintf(fid, "\\newcommand{\\ansY}{%s}\n", char(slau.y))
fprintf(fid, "\\newcommand{\\ansZ}{%s}\n", char(slau.z))

format rat

mpiOneX =   amatrix(1,1) / fmatrix(1, 1)
mpiOneY = - fmatrix(1,2) / fmatrix(1, 1)
mpiOneZ = - fmatrix(1,3) / fmatrix(1, 1)
mpiTwoX = - fmatrix(2,1) / fmatrix(2, 2)
mpiTwoY =   amatrix(2,1) / fmatrix(2, 2)
mpiTwoZ = - fmatrix(2,3) / fmatrix(2, 2)
mpiThrX = - fmatrix(3,1) / fmatrix(3, 3)
mpiThrY = - fmatrix(3,2) / fmatrix(3, 3)
mpiThrZ =   amatrix(3,1) / fmatrix(3, 3)

fprintf(fid, "\\newcommand{\\mpiOneX}{\\frac{%d}{%d}}\n",    amatrix(1,1), fmatrix(1, 1))
fprintf(fid, "\\newcommand{\\mpiOneY}{\\frac{%d}{%d}}\n",  - fmatrix(1,2), fmatrix(1, 1))
fprintf(fid, "\\newcommand{\\mpiOneZ}{\\frac{%d}{%d}}\n",  - fmatrix(1,3), fmatrix(1, 1))
fprintf(fid, "\\newcommand{\\mpiTwoX}{\\frac{%d}{%d}}\n",  - fmatrix(2,1), fmatrix(2, 2))
fprintf(fid, "\\newcommand{\\mpiTwoY}{\\frac{%d}{%d}}\n",    amatrix(2,1), fmatrix(2, 2))
fprintf(fid, "\\newcommand{\\mpiTwoZ}{\\frac{%d}{%d}}\n",  - fmatrix(2,3), fmatrix(2, 2))
fprintf(fid, "\\newcommand{\\mpiThrX}{\\frac{%d}{%d}}\n",  - fmatrix(3,1), fmatrix(3, 3))
fprintf(fid, "\\newcommand{\\mpiThrY}{\\frac{%d}{%d}}\n",  - fmatrix(3,2), fmatrix(3, 3))
fprintf(fid, "\\newcommand{\\mpiThrZ}{\\frac{%d}{%d}}\n",    amatrix(3,1), fmatrix(3, 3))

fclose (fid);

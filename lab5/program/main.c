#include <stdio.h>
#include <math.h>

void mpi()
{
	printf("\n Метод Простых Итераций\n");
	printf("k\tx_k\tx_k1\tx_k1 - x_k\ty_k\ty_k1\ty_k1 - y_k\tz_k\tz_k1\tz_k1 - z_k\n");
	int k = 0;
	double eps = 0.001;
	double x_k, y_k, z_k, x_k1 = -0.1/6.0, y_k1 = 0.5/-5.0, z_k1 = 6.0/6.0;

	do {
		y_k = y_k1;
		x_k = x_k1;
		z_k = z_k1;

		x_k1 = -0.1/6.0 + (0.125/6.0)*y_k + (0.1/6.0)*z_k;
		y_k1 = (3.0/-5.0)*x_k + 0.5/-5.0 + (-0.5/-5.0)*z_k;
		z_k1 = (-2.0/6.0)*x_k +(-3.0/6.0)*y_k + 6.0/6.0;

		printf("%d\t%.4f\t%.4f\t%.4f\t\t%.4f\t%.4f\t%.4f\t\t%.4f\t%.4f\t%.4f\n",
				k++, x_k, x_k1, x_k1 - x_k, y_k, y_k1, y_k1 - y_k, z_k, z_k1, z_k1 - z_k);
	} while (fabs(x_k1 - x_k) > eps || fabs(y_k1 - y_k) > eps || fabs(z_k1 - z_k) > eps);

	printf("res:\t%.4f\t%.4f\t%.4f\n", x_k, y_k, z_k);
}

void mzei()
{
	printf("\n Метод Зейделя\n");
	printf("k\tx_k\tx_k1\tx_k1 - x_k\ty_k\ty_k1\ty_k1 - y_k\tz_k\tz_k1\tz_k1 - z_k\n");
	int k = 0;
	double eps = 0.001;
	double x_k, y_k, z_k, x_k1 = -0.1/6.0, y_k1 = 0.5/-5.0, z_k1 = 6.0/6.0;

	do {
		y_k = y_k1;
		x_k = x_k1;
		z_k = z_k1;

		x_k1 = -0.1/6.0 + (0.125/6.0)*y_k + (0.1/6.0)*z_k;
		y_k1 = (3.0/-5.0)*x_k1 + 0.5/-5.0 + (-0.5/-5.0)*z_k;
		z_k1 = (-2.0/6.0)*x_k1 +(-3.0/6.0)*y_k1 + 6.0/6.0;

		printf("%d\t%.4f\t%.4f\t%.4f\t\t%.4f\t%.4f\t%.4f\t\t%.4f\t%.4f\t%.4f\n",
				k++, x_k, x_k1, x_k1 - x_k, y_k, y_k1, y_k1 - y_k, z_k, z_k1, z_k1 - z_k);
	} while (fabs(x_k1 - x_k) > eps || fabs(y_k1 - y_k) > eps || fabs(z_k1 - z_k) > eps);

	printf("res:\t%.4f\t%.4f\t%.4f\n", x_k, y_k, z_k);
}

int main() {
	mpi();
	mzei();
	return 0;
}

#include <stdio.h>
#include <math.h>

void nm () {
	printf("\n Метод Ньютона\n");
	printf("k\tx_k\tx_k1\tx_k1 - x_k\ty_k\ty_k1\ty_k1 - y_k\n");
	int k = 0;
	double eps = 0.001;
	double x_k, y_k, x_k1 = 1, y_k1 = 1;

	do {
		y_k = y_k1;
		x_k = x_k1;

		x_k1 = x_k - ( 1 * (pow(x_k, 2) + y_k - 6) + -1    * (x_k + y_k - 4)) / (2*x_k - 1);
		y_k1 = y_k - (-1 * (pow(x_k, 2) + y_k - 6) + 2*x_k * (x_k + y_k - 4)) / (2*x_k - 1);

		printf("%d\t%-7.4f\t%-7.4f\t%-7.4f\t\t%-7.4f\t%-7.4f\t%-7.4f\n",
				k++, x_k, x_k1, x_k1 - x_k, x_k, y_k1, y_k1 - y_k);
	} while (fabs(x_k1 - x_k) > eps || fabs(y_k1 - y_k) > eps);

	printf("res:\t%.4f\t%.4f\n", x_k, y_k);
}


void mpi()
{
	printf("\n Метод Простых Итераций\n");
	printf("k\tx_k\tx_k1\tx_k1 - x_k\ty_k\ty_k1\ty_k1 - y_k\n");
	int k = 0;
	double eps = 0.001;
	double x_k, y_k, x_k1 = 1, y_k1 = 1;

	do {
		y_k = y_k1;
		x_k = x_k1;

		x_k1 = x_k - 1 * (pow(x_k, 2) + y_k - 6) + 1 * (x_k + y_k - 4);
		y_k1 = y_k + 1 * (pow(x_k, 2) + y_k - 6) - 2 * (x_k + y_k - 4);

		printf("%d\t%-7.4f\t%-7.4f\t%-7.4f\t\t%-7.4f\t%-7.4f\t%-7.4f\n",
				k++, x_k, x_k1, x_k1 - x_k, x_k, y_k1, y_k1 - y_k);
	} while (fabs(x_k1 - x_k) > eps || fabs(y_k1 - y_k) > eps);

	printf("res:\t%-7.4f\t%-7.4f\n", x_k, y_k);
}


int main() {
	mpi();
	nm();
	return 0;
}

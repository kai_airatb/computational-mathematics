pkg load symbolic
syms x y a b g d
fid = fopen ('out.tex', "w");

#Me
f1 = x^2 + y - 6
f2 = x + y - 4
begin = [1, 1]

##primer
#f1 = x + y - 5
#f2 = x * y - 6
#begin = [2, 1]

#Albert 7
#f1 = 2*x + y - 5
#f2 = y - x^(2) - 2
#begin = [1, 1]

##Airat 8
#f1 = x - 3 * y - 5
#f2 = x - y^(2) - 1
#begin = [-2, -1]

#jaha 20
#f1 = x^{2} - 4
#f2 = x + 3 * y - 8
#begin = [1.8, 1]

fprintf(fid, "\\newcommand{\\formulaOne}{%s}\n", latex(f1))
fprintf(fid, "\\newcommand{\\formulaTwo}{%s}\n", latex(f2))
fprintf(fid, "\\newcommand{\\beginPrib}{(%d, %d)}\n", begin(1), begin(2))
an = solve(f1 == 0, f2 == 0)
fprintf(fid, "\\newcommand{\\analitics}{")
for i = 1:size(an, 2)
	fprintf(fid, "(%s, %s)", char(an{i}.x), char(an{i}.y))
	if i < (size(an, 2) - 1)
		fprintf(fid, ", ")
	endif
	if i == (size(an, 2) - 1)
		fprintf(fid, "\\text{ и }")
	endif
endfor
fprintf(fid, "}\n")


fodx = diff(f1, x)
fprintf(fid, "\\newcommand{\\fodx}{%s}\n", latex(fodx))
if strcmp(latex(fodx), "1") == 0
	fprintf(fid, "\\newcommand{\\fodxs}{%s}\n", latex(fodx))
else
	fprintf(fid, "\\newcommand{\\fodxs}{}\n")
endif

fody = diff(f1, y)
fprintf(fid, "\\newcommand{\\fody}{%s}\n", latex(fody))
if strcmp(latex(fody), "1") == 0
	fprintf(fid, "\\newcommand{\\fodys}{%s}\n", latex(fody))
else
	fprintf(fid, "\\newcommand{\\fodys}{}\n")
endif

ftdx = diff(f2, x)
fprintf(fid, "\\newcommand{\\ftdx}{%s}\n", latex(ftdx))
if strcmp(latex(ftdx), "1") == 0
	fprintf(fid, "\\newcommand{\\ftdxs}{%s}\n", latex(ftdx))
else
	fprintf(fid, "\\newcommand{\\ftdxs}{}\n")
endif

ftdy = diff(f2, y)
fprintf(fid, "\\newcommand{\\ftdy}{%s}\n", latex(ftdy))
if strcmp(latex(ftdy), "1") == 0
	fprintf(fid, "\\newcommand{\\ftdys}{%s}\n", latex(ftdy))
else
	fprintf(fid, "\\newcommand{\\ftdys}{}\n")
endif

fodx = diff(f1, x)
fody = diff(f1, y)
ftdx = diff(f2, x)
ftdy = diff(f2, y)

slauo = solve(
	1 + a*fodx + b*ftdx == 0,
		g*fodx + d*ftdx == 0,
		a*fody + b*ftdy == 0,
	1 + g*fody + d*ftdy == 0
)

fprintf(fid, "\\newcommand{\\slauoa}{%s}\n", latex(slauo.a))
fprintf(fid, "\\newcommand{\\slauob}{%s}\n", latex(slauo.b))
fprintf(fid, "\\newcommand{\\slauog}{%s}\n", latex(slauo.g))
fprintf(fid, "\\newcommand{\\slauod}{%s}\n", latex(slauo.d))

xko = (x + slauo.a * (f1) - slauo.b * (f2))
yko = (y + slauo.g * (f1) - slauo.d * (f2))
fprintf(fid, "\\newcommand{\\xko}{%s}\n",
	strrep(strrep(latex(xko), "x", "x_k"), "y", "y_k"))
fprintf(fid, "\\newcommand{\\yko}{%s}\n",
	strrep(strrep(latex(yko), "x", "x_k"), "y", "y_k"))

xkob = simplify(expand(xko))
ykob = simplify(expand(yko))

fprintf(fid, "\\newcommand{\\xkob}{%s}\n",
	strrep(strrep(latex(xkob), "x", "x_k"), "y", "y_k"))
fprintf(fid, "\\newcommand{\\ykob}{%s}\n",
	strrep(strrep(latex(ykob), "x", "x_k"), "y", "y_k"))

fodxn = subs(subs(fodx, x, begin(1)), y, begin(2))
fprintf(fid, "\\newcommand{\\fodxn}{%s}\n", latex(fodxn))
if strcmp(latex(fodxn), "1") == 0
	fprintf(fid, "\\newcommand{\\fodxns}{%s}\n", latex(fodxn))
else
	fprintf(fid, "\\newcommand{\\fodxns}{}\n")
endif

fodyn = subs(subs(fody, y, begin(2)), x, begin(1))
fprintf(fid, "\\newcommand{\\fodyn}{%s}\n", latex(fodyn))
if strcmp(latex(fodyn), "1") == 0
	fprintf(fid, "\\newcommand{\\fodyns}{%s}\n", latex(fodyn))
else
	fprintf(fid, "\\newcommand{\\fodyns}{}\n")
endif

ftdxn = subs(subs(ftdx, x, begin(1)), y, begin(2))
fprintf(fid, "\\newcommand{\\ftdxn}{%s}\n", latex(ftdxn))
if strcmp(latex(ftdxn), "1") == 0
	fprintf(fid, "\\newcommand{\\ftdxns}{%s}\n", latex(ftdxn))
else
	fprintf(fid, "\\newcommand{\\ftdxns}{}\n")
endif

ftdyn = subs(subs(ftdy, y, begin(2)), x, begin(1))
fprintf(fid, "\\newcommand{\\ftdyn}{%s}\n", latex(ftdyn))
if strcmp(latex(ftdyn), "1") == 0
	fprintf(fid, "\\newcommand{\\ftdyns}{%s}\n", latex(ftdyn))
else
	fprintf(fid, "\\newcommand{\\ftdyns}{}\n")
endif

slaut = solve(
	1 + a*char(fodxn) + b*char(ftdxn) == 0,
		g*char(fodxn) + d*char(ftdxn) == 0,
		a*char(fodyn) + b*char(ftdyn) == 0,
	1 + g*char(fodyn) + d*char(ftdyn) == 0
)

fprintf(fid, "\\newcommand{\\slauta}{%s}\n", latex(slaut.a))
fprintf(fid, "\\newcommand{\\slautb}{%s}\n", latex(slaut.b))
fprintf(fid, "\\newcommand{\\slautg}{%s}\n", latex(slaut.g))
fprintf(fid, "\\newcommand{\\slautd}{%s}\n", latex(slaut.d))

f1texk = strrep(strrep(latex(f1), "x", "x_k"), "y", "y_k")
f2texk = strrep(strrep(latex(f2), "x", "x_k"), "y", "y_k")

fprintf(fid, "\\newcommand{\\fotexk}{%s}\n", f1texk);
fprintf(fid, "\\newcommand{\\fttexk}{%s}\n", f2texk);

W = [fodx, fody;
	 ftdx, ftdy]

Wo = W^{-1}
Wom = Wo * det(W)

fprintf(fid, "\\newcommand{\\ndetw}{%s}\n", latex(det(W)));
fprintf(fid, "\\newcommand{\\nothw}{%s}\n", strrep(strrep(latex(simplify(Wom)), "[", "("), "]", ")"));


fprintf(fid, "\\newcommand{\\nendfx}{%s}\n", latex(simplify(x - (Wo(1, 1) * f1 + Wo(1, 2) * f2))));
fprintf(fid, "\\newcommand{\\nendfy}{%s}\n", latex(simplify(y - (Wo(2, 1) * f1 + Wo(2, 2) * f2))));


fclose (fid);
